package repository

import (
	"encoding/json"
	"jwt-auth/config"
	"jwt-auth/model"
)

type IUserRepository interface {
	FindUserByUsername(username string) (model.User, error)
}

type redisUserStruct struct{}

func RedisUserRepository() *redisUserStruct {
	return &redisUserStruct{}
}

func (ruStruct redisUserStruct) FindUserByUsername(username string) (*model.User, error) {
	resp, err := config.GetRedisDB().Get(config.GetDBContext(), username).Bytes()
	if err != nil {
		return &model.User{}, err
	}
	userRequest := new(model.User)
	json.Unmarshal(resp, userRequest)
	return userRequest, nil
}
