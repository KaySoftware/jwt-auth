package service

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"jwt-auth/model"
	"log"
	"os"
	"time"
)

type JWTService interface {
	GenerateToken(username string, password string, isUser bool) *model.TokenResponse
	ValidateToken(token string) (*jwt.Token, error)
}

type authCustomClaims struct {
	Name string `json:"name"`
	User bool   `json:"user"`
	jwt.StandardClaims
}

type jwtServices struct {
	secretKey string
	issure    string
}

func JWTAuthService() JWTService {
	return &jwtServices{
		secretKey: getSecretKey(),
		issure:    "Kay",
	}
}

func getSecretKey() string {
	secret := os.Getenv("SECRET")
	if secret == "" {
		secret = "secret"
	}
	return secret
}

func (service *jwtServices) GenerateToken(username string, password string, isUser bool) *model.TokenResponse {
	var expireTime = time.Now().Add(time.Hour * 48).Unix()
	claims := &authCustomClaims{
		Name: username,
		User: isUser,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireTime,
			Issuer:    service.issure,
			IssuedAt:  time.Now().Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	//encoded string
	t, err := token.SignedString([]byte(service.secretKey))
	if err != nil {
		panic(err)
	}
	return &model.TokenResponse{
		Token:     t,
		ExpiresAt: expireTime,
	}
}

func (service *jwtServices) ValidateToken(encodedToken string) (*jwt.Token, error) {
	log.Println("Tokend entered " + encodedToken)
	return jwt.Parse(encodedToken, func(token *jwt.Token) (interface{}, error) {
		if _, isvalid := token.Method.(*jwt.SigningMethodHMAC); !isvalid {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(service.secretKey), nil
	})
}
