package service

import (
	"jwt-auth/model"
	"jwt-auth/repository"
)

type IUserService interface {
	FindUserByUsername(username string) (model.User, error)
}

type redisUserStruct struct{}

func UserService() *redisUserStruct {
	return &redisUserStruct{}
}

func (ruStruct *redisUserStruct) FindUserByUsername(username string) (*model.User, error) {
	return repository.RedisUserRepository().FindUserByUsername(username)
}
