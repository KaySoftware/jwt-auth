package model

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
	IsUser   bool   `json:"isUser"`
}

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type TokenResponse struct {
	Token     string `json:"token"`
	ExpiresAt int64  `json:"expires_at"`
}
