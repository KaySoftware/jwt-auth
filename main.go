package main

import (
	"github.com/gin-gonic/gin"
	"jwt-auth/config"
	"jwt-auth/controller"
	"jwt-auth/middleware"
	"log"
)

func main() {
	//Initializing configuration file
	config.InitializeAppConfiguration()
	//Initializing auth database configuration and more
	config.InitAuthConfig()
	//Initalize roles and permissions
	config.InitializeRolesAndPermissions()

	//Initializing HTTP Gin Framework
	log.Println("INITIALIZING HTTP WEB APPLICATION")
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.GET("/", controller.MainEndpoint)
	rAuth := router.Group("/")
	rAuth.POST("/oauth/token", controller.GenerateTokenEndpoint)
	rAuth.Use(middleware.Authentication())
	{
		rAuth.POST("/home", controller.HomeEndpoint)
	}
	router.Run(config.AppCfg.Server.Host + ":" + config.AppCfg.Server.Port)
	log.Println("FINISHED HTTP WEB APPLICATION")
}
