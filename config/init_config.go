package config

import (
	"gopkg.in/yaml.v2"
	"log"
	"os"
	"strings"
)

var AppCfg Application

func InitializeAppConfiguration() error {
	log.Println("INITIALIZING APPLICATION, INITIAL CONFIGURATION")
	envVal, isPresent := os.LookupEnv("APP_ENVIRONMENT")
	if isPresent {
		f, err := os.Open("application-" + strings.ToLower(envVal) + ".yml")
		if err != nil {
			log.Println("Not application-" + strings.ToLower(envVal) + ".yml trying to find main yml file")
			f2, err2 := os.Open("application.yml")
			if err2 != nil {
				log.Fatalln("No application.yml file found, exiting")
				return err2
			}
			attachConfigToEntity(f2)
			defer f2.Close()
		} else {
			attachConfigToEntity(f)
			defer f.Close()
		}
	} else {
		log.Println("NO APP_ENVIRONMENT Found, using the base config file")
		f2, err2 := os.Open("application.yml")
		if err2 != nil {
			log.Fatalln("No application.yml file found, exiting")
			return err2
		} else {
			attachConfigToEntity(f2)
			defer f2.Close()
		}
	}
	log.Println("FINISHED CONFIGURATION FILE SUCESSFULLY")
	return nil
}

func attachConfigToEntity(f *os.File) error {
	decoder := yaml.NewDecoder(f)
	err := decoder.Decode(&AppCfg)
	if err != nil {
		return err
	}
	return nil
}
