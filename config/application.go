package config

type Application struct {
	Server struct {
		Host string `yaml:"host"`
		Port string `yaml:"port"`
	}

	Auth struct {
		Database struct {
			Host     string `yaml:"host"`
			Port     string `yaml:"port"`
			Password string `yaml:"password"`
			DB       int    `yaml:"db"`
		}
		CreateInitialAdmin bool `yaml:"createInitialAdmin"`
	}
}
