package config

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"jwt-auth/model"
	"jwt-auth/util"
	"log"
	"strconv"
)

var (
	ctx = context.Background()
	Rdb *redis.Client
)

func InitAuthConfig() error {
	log.Println("INITIALIZING DATABASE")
	rdb := GetRedisDB()
	ping, err := rdb.Ping(ctx).Result()
	if err != nil {
		log.Fatalln("Error pinging server: " + err.Error())
		return err
	}
	log.Println("Pinged succesfully server with: " + ping)
	log.Println("DATABASE INITIALIZED SUCCESFULLY")
	log.Println("Create initial admin " + strconv.FormatBool(AppCfg.Auth.CreateInitialAdmin))
	if AppCfg.Auth.CreateInitialAdmin {
		return createAdminUser()
	}
	return nil
}

func GetRedisDB() *redis.Client {
	if Rdb == nil {
		Rdb = redis.NewClient(&redis.Options{
			Addr:     AppCfg.Auth.Database.Host + ":" + AppCfg.Auth.Database.Port,
			Password: AppCfg.Auth.Database.Password,
			DB:       AppCfg.Auth.Database.DB,
		})
	}
	return Rdb
}

func GetDBContext() context.Context {
	return ctx
}

func createAdminUser() error {
	sha := util.TextToSha256("123456")
	userRequest := &model.User{
		Username: "admin",
		Password: sha,
		IsUser:   true,
	}
	jsonConvert, _ := json.Marshal(userRequest)
	log.Println("Creating admin user...")
	rdb := GetRedisDB()
	rdb.Del(ctx, "admin")
	res := rdb.Set(ctx, "admin", jsonConvert, 0)
	if res.Err() != nil {
		log.Fatalln("Failed created admin user: " + res.Err().Error())
		return res.Err()
	}
	log.Println("Created admin user...")
	return nil
}
