package config

import (
	"github.com/casbin/casbin/v2"
	"log"
)

var roleValidator *casbin.Enforcer

func InitializeRolesAndPermissions() error {
	log.Println("INITIALIZING ROLES AND PERMISSIONS")
	validator, err := casbin.NewEnforcer("./file/rbac.conf", "./file/rbac.csv")
	if err != nil {
		log.Fatalln("Error getting roles and permission configuration. " + err.Error())
		return err
	}
	roleValidator = validator
	log.Println("SUCCESFULLY LOADED ROLES AND PERMISSIONS")
	return nil
}

func GetEnforcer() *casbin.Enforcer {
	return roleValidator
}
