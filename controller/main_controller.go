package controller

import "github.com/gin-gonic/gin"

/*
	Starting principal controllers
*/
func MainEndpoint(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"message": "pong",
	})
}

func HomeEndpoint(ctx *gin.Context) {
	ctx.JSON(200, gin.H{
		"message": "welcome guys!",
	})
}
