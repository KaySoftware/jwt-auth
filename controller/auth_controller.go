package controller

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"jwt-auth/model"
	"jwt-auth/service"
	"jwt-auth/util"
	"net/http"
)

var jwtService = service.JWTAuthService()
var loginRequest *model.LoginRequest

func GenerateTokenEndpoint(ctx *gin.Context) {
	err := ctx.ShouldBind(&loginRequest)
	if err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, "{\"Error\":\"Can't recreate the bean\"}")
	}
	userRequest, err := service.UserService().FindUserByUsername(loginRequest.Username)
	if err != nil || userRequest.Username == "" || userRequest.Password != util.TextToSha256(loginRequest.Password) {
		errInfo, _ := json.Marshal(err)
		errorResponse := model.ErrorResponse{
			Code:    1,
			Message: "Error finding user",
			Data:    errInfo,
		}
		ctx.JSON(http.StatusUnprocessableEntity, errorResponse)
	} else {
		ctx.JSON(http.StatusOK, jwtService.GenerateToken(userRequest.Username, userRequest.Password, userRequest.IsUser))
	}
}

func LogoutEndpoint(ctx *gin.Context) {

}
