package util

import (
	"crypto/sha256"
	"encoding/hex"
)

func TextToSha256(text string) string {
	h := sha256.New()
	h.Write([]byte(text))
	return hex.EncodeToString(h.Sum(nil))
}
