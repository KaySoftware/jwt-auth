package middleware

import (
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"jwt-auth/service"
	"log"
	"net/http"
)

func Authentication() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		log.Println("VALIDATING TOKEN OBTAINED...")
		const BEARER = "Bearer "
		authHeader := ctx.GetHeader("Authorization")
		if authHeader == "" {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": "You are not authorized to enter here",
			})
		}
		tokenString := authHeader[len(BEARER):]
		token, err := service.JWTAuthService().ValidateToken(tokenString)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
				"message": err.Error(),
			})
		} else if token != nil && token.Valid {
			claims := token.Claims.(jwt.MapClaims)
			fmt.Println(claims)
			log.Println("TOKEN VALIDATED CORRECTLY")
			userRequest, err := service.UserService().FindUserByUsername(claims["name"].(string)) //casting to string
			if err != nil || userRequest.Username == "" {
				ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
					"message": err.Error(),
				})
			} else {
				ctx.Set("userRequest", userRequest)
				ctx.Next()
			}
		}
	}
}
